import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { environment} from '../../environments/environment';
import { TranslateModule, TranslateService } from '@ngx-translate/core';

import { ProfileHomeComponent } from './components/profile-home/profile-home.component';
import { ProfileRoutingModule} from './profile-routing.module';
import { ProfilePreviewComponent } from './components/profile-preview/profile-preview.component';

import {NgPipesModule} from 'ngx-pipes';
import {NgArrayPipesModule} from 'angular-pipes';


@NgModule({
  imports: [
    CommonModule,
    ProfileRoutingModule,
    TranslateModule,
    NgPipesModule,
    NgArrayPipesModule
  ],
  declarations: [ProfileHomeComponent, ProfilePreviewComponent]
})
export class ProfileModule {
  constructor(private _translateService: TranslateService) {
    environment.languages.forEach((culture) => {
      import(`./i18n/profile.${culture}.json`).then((translations) => {
        this._translateService.setTranslation(culture, translations, true);
      });
    });
  }
}
