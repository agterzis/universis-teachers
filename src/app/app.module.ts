import { BrowserModule, Title } from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';
import {APP_INITIALIZER, CUSTOM_ELEMENTS_SCHEMA, NgModule, LOCALE_ID, NO_ERRORS_SCHEMA} from '@angular/core';
import { AppComponent } from './app.component';
import { FullLayoutComponent } from './layouts/full-layout.component';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {environment} from '../environments/environment';
import {LocationStrategy, HashLocationStrategy, registerLocaleData} from '@angular/common';
import {AngularDataContext, ClientDataContextConfig, DATA_CONTEXT_CONFIG, MostModule} from '@themost/angular';
import {ConfigurationService, APP_LOCATIONS} from '@universis/common';
import {SharedModule} from '@universis/common';
import {ErrorModule} from '@universis/common';
import {AuthModule} from '@universis/common';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import {TooltipModule} from 'ngx-bootstrap';
import {AppSidebarModule} from '@coreui/angular';
import * as locations from './app.locations';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';


// Routing Module
import {AppRoutingModule} from './app.routing';
import {BreadcrumbsComponent} from './layouts/breadcrumb.component';
import { ProfileService } from './profile/services/profile.service';
import { ThesesModule } from './theses/theses.module';
import { StudentsModule } from './students/students.module';
import {ModalModule} from 'ngx-bootstrap';
import { TeachersSharedModule } from './teachers-shared/teachers-shared.module';
import {MessageSharedService} from './teachers-shared/services/messages.service';
import { QaModule } from '@universis/ngx-qa';

import { ConsultedStudentsSharedModule } from './consulted-students/consulted-students.shared';
import { StoreModule } from '@ngrx/store';
import { metaReducers, reducers } from './reducers';
import * as fromTables from '@universis/ngx-tables';


@NgModule({
  declarations: [
    AppComponent,
    FullLayoutComponent,
    BreadcrumbsComponent,
  ],
  imports: [
    QaModule,
    ChartsModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MostModule.forRoot({
      base: '/',
      options: {
          useMediaTypeExtensions: false,
          useResponseConversion: true
      }
    }),
    TranslateModule.forRoot(),
    SharedModule.forRoot(),
    TeachersSharedModule.forRoot(),
    AuthModule.forRoot(),
    AppRoutingModule,
    ErrorModule.forRoot(),
    BsDropdownModule.forRoot(),
    ThesesModule,
    StudentsModule,
    AppSidebarModule,
    ModalModule.forRoot(),
    TooltipModule.forRoot(),
    StoreModule.forRoot(reducers, { metaReducers }),
    StoreModule.forFeature(fromTables.featureName, fromTables.tableReducers),
    ConsultedStudentsSharedModule
  ],
  providers: [
    Title,
    {
        provide: APP_LOCATIONS, useValue: locations.TEACHERS_APP_LOCATIONS
    },
    {
        provide: APP_INITIALIZER,
        // use APP_INITIALIZER to load application configuration
        useFactory: (configurationService: ConfigurationService) =>
            () => {
                // load application configuration
                return configurationService.load().then(loaded => {
                    // load angular locales
                    const sources = configurationService.settings.i18n.locales.map(locale => {
                        return import(`@angular/common/locales/${locale}.js`).then(module => {
                            // register locale data
                            registerLocaleData(module.default);
                            // return
                            return Promise.resolve();
                        });
                    });
                    return Promise.all(sources).then(() => {
                        // return true for APP_INITIALIZER
                        return Promise.resolve(true);
                    });
                });
            },
        deps: [ConfigurationService],
        multi: true
    },
    {
        provide: LOCALE_ID,
        useFactory: (configurationService: ConfigurationService) => {
            return configurationService.currentLocale;
        },
        deps: [ConfigurationService]
    },
    {
        provide: LocationStrategy,
        useClass: HashLocationStrategy
    },
    ProfileService,
    MessageSharedService
    ],
  bootstrap: [AppComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
})
export class AppModule {
  constructor() {
    }
}
