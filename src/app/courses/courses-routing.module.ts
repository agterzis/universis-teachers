import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AuthGuard} from '@universis/common';
import {CoursesHomeComponent} from './components/courses-home/courses-home.component';
import {CoursesRecentComponent} from './components/courses-recent/courses-recent.component';
import {CoursesDetailsComponent} from './components/courses-details/courses-details.component';
import {CoursesDetailsGeneralComponent} from './components/courses-details/courses-details-general.component';
import { CourseClassAbsenceLimitComponent } from './components/courses-details/course-class-absence-limit.component';
import {CoursesHistoryComponent} from './components/courses-history/courses-history.component';
import {CoursesDetailsGradingComponent} from './components/courses-details/courses-details-grading.component';
import {CoursesDetailsStudentsComponent} from './components/courses-details/courses-details-students.component';
import {CoursesDetailsExamComponent} from './components/courses-details/course-details-exam.component';
import { CoursesDetailsGradingIndexComponent } from './components/courses-details/courses-details-grading-index';
import {EventsLibComponent, AbsencesComponent, EventAbsencesComponent, DeleteEventComponent} from '@universis/ngx-events';
import {
  DeleteEventModelResolver,
  EventAbsencesResolver,
  EventConfigResolver,
  EventCourseClassInstructorResolver,
  EventCourseClassResolver, EventCourseClassSectionEndpointResolver, EventModelResolver,
  ShowActionButtonResolver
} from './event-configs.resolver';
import { ExamsNewGradeSubmissionComponent } from './components/exams-new-grade-submission/exams-new-grade-submission.component';
import {CourseClassResolver} from './course-class.revolver';
import { CourseOutlineComponent } from '@universis/ngx-qa';
import { CourseOutlineEditComponent } from '@universis/ngx-qa';
import { CourseOutlineRootComponent } from '@universis/ngx-qa';
import { CourseEvaluationsListComponent } from '@universis/ngx-qa';
import { CourseEvaluationComponent } from '@universis/ngx-qa';
import {AvailableServiceGuard} from '../available-service.guard';
import {CoursesExamsHistoryComponent} from './components/courses-exams-history/courses-exams-history.component';

const routes: Routes = [
  {
    path: '',
    component: CoursesHomeComponent,
    canActivate: [
      AuthGuard
    ],
    canActivateChild: [
      AuthGuard
    ],
    children: [
      {
        path: '',
        redirectTo: 'recent',
        pathMatch: 'full'
      },
      {
        path: 'history',
        component: CoursesHistoryComponent
      },
      {
        path: 'examshistory',
        component: CoursesExamsHistoryComponent
      },
      {
        path: ':course/:year/:period',
        component: CoursesDetailsComponent,
        children: [
          {
            path: '',
            redirectTo: 'details',
            pathMatch: 'full'
          },
          {
            path: 'details',
            component: CoursesDetailsGeneralComponent,
            children: [
              {
                path: ':id',
                pathMatch: 'full',
                component: CourseClassAbsenceLimitComponent,
                outlet: 'modal',
                resolve: {
                  courseClass: CourseClassResolver
                }
              }
            ]
          },
          {
            path: 'students',
            component: CoursesDetailsStudentsComponent
          },
          {
            path: 'exams',
            component: CoursesDetailsGradingComponent,
            children: [
              {
                'path': '',
                'pathMatch': 'full',
                component: CoursesDetailsGradingIndexComponent
              },
              {
                path: ':courseExam',
                component: CoursesDetailsExamComponent
              },
              {
                path: ':courseExam/new-submission',
                component: ExamsNewGradeSubmissionComponent
              }
            ]
          },
          {
            path: 'teachingEvents/:id/attendance',
            component: EventAbsencesComponent,
            resolve: {
              _model: EventAbsencesResolver,
              eventModel: EventModelResolver
            }
          },
          {
            path: 'teachingEvents',
            component: EventsLibComponent,
            resolve: {
              model: EventConfigResolver,
              newModel: EventConfigResolver,
              courseClass: EventCourseClassResolver,
              showActions: ShowActionButtonResolver,
              instructor: EventCourseClassInstructorResolver,
              sectionEndpoint: EventCourseClassSectionEndpointResolver,
              actionModel: EventConfigResolver
            },
            children: [
              {
                path: ':id/delete',
                component: DeleteEventComponent,
                resolve: {
                  _model: DeleteEventModelResolver,
                }
              },
            ]
          },
          {
            path: 'outline',
            component: CourseOutlineRootComponent,
            canActivate: [
              AvailableServiceGuard
            ],
            data : {
              serviceDependencies: [
                'QualityAssuranceService'
              ]
            },
            resolve: {
              courseClass: CourseClassResolver
            },
            children: [
              {
                path: '',
                redirectTo: 'view',
                pathMatch: 'full'
              },
              {
                path: 'view',
                component: CourseOutlineComponent
              },
              {
                path: 'edit',
                component: CourseOutlineEditComponent
              }
            ]
          },
          {
            path: 'evaluation',
            component: CourseEvaluationComponent,
            canActivate: [
              AvailableServiceGuard
            ],
            data : {
              serviceDependencies: [
                'QualityAssuranceService'
              ]
            },
            resolve: {
              courseClass: CourseClassResolver
            }
          },
        ]
      },
      {
        path: 'recent',
        component: CoursesRecentComponent
      },
      {
        path: 'evaluations',
        component: CourseEvaluationsListComponent
      }

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CoursesRoutingModule { }
