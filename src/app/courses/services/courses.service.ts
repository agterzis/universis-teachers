import { Injectable } from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {ConfigurationService, asyncMemoize} from '@universis/common';
import {BehaviorSubject} from 'rxjs/internal/BehaviorSubject';
import {HttpClient, HttpHeaders, HttpRequest} from '@angular/common/http';
import {ClientDataQueryable} from '@themost/client';

@Injectable({
  providedIn: 'root'
})
export class CoursesService {

  sharingData = { id: '' , year: '' , period: '' };
  requestTimeout = '120000';

  constructor(private _context: AngularDataContext,
              private _configuration: ConfigurationService,
              private _http: HttpClient) {
    if (
      this._configuration
      && this._configuration.settings
      && this._configuration.settings.app
      && !isNaN((<any>this._configuration.settings.app).requestTimeout)
    ) {
      this.requestTimeout = (<any>this._configuration.settings.app).requestTimeout;
    }
  }

  saveData(value1, value2, value3) {
    this.sharingData.id = value1;
    this.sharingData.year = value2;
    this.sharingData.period = value3;
  }

  getAllInstructors(): any {
    return this._context.model('instructors/me')
      .asQueryable()
      .expand('department')
      .getItems();
  }

  getCourseExamSelectValues(skip: number = 0, take: number = -1): any {
    return this._context.model('instructors/me/exams')
      .asQueryable()
      .groupBy('status/alternateName', 'year/alternateName', 'examPeriod/alternateName')
      .select('status/alternateName as status', 'year/alternateName as year', 'examPeriod/alternateName as examPeriod')
      .orderByDescending('year/alternateName')
      .getItems();
  }

 @asyncMemoize()
  getCourseExams(): any {
    return this._context.model('instructors/me/exams')
      .asQueryable()
      .expand('examPeriod,course,status,classes')
      .orderByDescending('year')
      .thenByDescending('examPeriod')
      .getList();
  }

  searchCourseExams(searchText: string = '', skip: number = 0, take: number = -1) {
    return this._context.model(`instructors/me/exams`)
      .asQueryable()
      // tslint:disable-next-line:max-line-length
      .expand('status,examPeriod,classes($expand=courseExam,courseClass($expand=period);$orderby=courseClass/period/id desc)),period,year,course($expand=department)')
      .where('name').contains(searchText)
      .or('course/displayCode').contains(searchText)
      .orderByDescending('year')
      .thenByDescending('examPeriod')
      .skip(skip)
      .take(take)
      .prepare();
  }
  getCourseExam(courseExam): any {
    return this._context.model('instructors/me/exams')
      .asQueryable()
      // tslint:disable-next-line: max-line-length
      .expand('status,examPeriod,course($expand=department($expand=departmentConfiguration)),classes($expand=courseExam,courseClass($expand=year,period))')
      .orderByDescending('year')
      .thenByDescending('examPeriod')
      .where('id').equal(courseExam)
      .getItem();
  }

  getCourseClassExams(courseClass: any): any {
    return this._context.model('instructors/me/exams')
      .asQueryable()
      .expand('examPeriod,course,status')
      .orderByDescending('year')
      .thenByDescending('examPeriod')
      .where('classes/courseClass').equal(courseClass)
      .getItems();
  }

  getCourseExams2(value): any {
    return this._context.model('instructors/me/exams/' + value + '/students')
      .asQueryable()
      .expand('courseType,student($expand=studentStatus),courseClass($expand=period,status,course($expand=department))')
      .getList();
  }

  getRecentCourses() {
    return this._context.model('instructors/me/currentClasses')
      .asQueryable()
      .expand('year,period,sections,course($expand=department)')
      .orderBy('course/department/name')
      .thenBy('title')
      .take(-1)
      .getItems();
  }

  getCourseCurrentExams(): any {
    return this._context.model('instructors/me/currentExams')
      .asQueryable()
      .expand('status,examPeriod,course($expand=department),classes($expand=courseExam,courseClass($expand=year,period))')
      .orderByDescending('year')
      .thenByDescending('examPeriod')
      .thenBy('name')
      .take(-1)
      .getItems();
  }

  getAllClasses(): any {
    return this._context.model('instructors/me/classes')
      .asQueryable()
      .expand('course($expand=department,instructor),period')
      .orderByDescending('year')
      .thenByDescending('period')
      .thenBy('title')
      .take(-1)
      .getItems();
  }

 getCourseClass(course, year, period): any {
    return this._context.model('instructors/me/classes')
      .asQueryable()
      // tslint:disable-next-line: max-line-length
      .expand('department,period,status,sections,instructors($expand=instructor($select=InstructorSummary)),course($expand=department,instructor)')
      .orderByDescending('year')
      .thenByDescending('period')
      .where('course').equal(course).and('year').equal(year).and('period').equal(period)
      .getItem();
  }

  getCourseClassStudents(courseClass, skip, take): ClientDataQueryable {
    return this._context.model(`instructors/me/classes/${encodeURIComponent(courseClass)}/students`)
      .asQueryable()
      .expand('courseClass,student($expand=department), status')
      .orderBy('student/person/familyName')
      .thenBy('student/person/givenName')
      .skip(skip)
      .take(take);
  }
  searchCourseClassStudents(courseClass: any, searchText: string, skip, take) {
    return this._context.model(`instructors/me/classes/${encodeURIComponent(courseClass)}/students`)
        .asQueryable().expand('courseClass, student($expand=department), status')
        .where('student/person/familyName').contains(searchText)
        .or('student/studentIdentifier').contains(searchText)
        .or('student/person/givenName').contains(searchText)
        .skip(skip)
        .take(take)
        .prepare();
  }

  getCourseClassList(course): any {
    return this._context.model('instructors/me/classes')
      .asQueryable()
      .orderByDescending('year')
      .thenByDescending('period')
      .expand('year', 'period', 'course')
      .where('course').equal(course)
      .select('id', 'course', 'title', 'year', 'period')
      .take(-1)
      .getItems();
  }

  getStudentList(value): any {
    return this._context.model('instructors/me/exams/' + value + '/students/export')
      .asQueryable()
      .getList();
  }

  getStudentBySearch(value, searchText): any {
    return this._context.model('instructors/me/exams/' + value + '/students')
      .asQueryable()
      .expand('courseType,student($expand=studentStatus),courseClass($expand=period,status,course($expand=department))')
      .where('student/person/familyName').contains(searchText)
      .or('student/studentIdentifier').contains(searchText)
      .or('student/person/givenName').contains(searchText)
      .take(-1)
      .getItems();
  }

  uploadGradesFile(file, courseExamId) {
    const formData: FormData = new FormData();
    formData.append('file', file, file.name);

    // get context service headers
    const serviceHeaders = this._context.getService().getHeaders();
    const postUrl = this._context.getService().resolve(`instructors/me/exams/${courseExamId}/students/import`);

    return this._http.post(postUrl, formData, {
      headers: serviceHeaders
    });
  }

  getCurrentCourseExams(courseExamId): any {
    return this._context.model('instructors/me/exams')
      .asQueryable()
      .expand('course($expand=department),examPeriod,status, gradeScale($expand=values)')
      .where('id').equal(courseExamId)
      .getItem();
  }

  /**
   *
   * Returns a list of open exams from a year until the current year of the
   * instructor's department
   *
   * @param {number} year The year to start the search
   */
  getOpenExamsSinceYear(year: number)  {
    return this._context.model('instructors/me/exams')
      .asQueryable()
      .where('status/alternateName').notEqual('closed')
      .and('year').greaterOrEqual(year)
      .expand('status,examPeriod,course($expand=department),classes($expand=courseExam,courseClass($expand=year,period))')
      .orderByDescending('year')
      .thenByDescending('examPeriod')
      .thenBy('name')
      .take(10)
      .getItems();
  }

  setUploadToComplete(courseExamId, uploadID): any {
    this._context.getService().setHeader('timeout', this.requestTimeout);
    return this._context.model(`instructors/me/exams/${courseExamId}/actions/${uploadID}/complete`)
      .save(null);
  }

  getUploadHistory (courseExamId): any {
    return this._context.model(`/instructors/me/exams/${courseExamId}/actions`)
      .asQueryable()
      .expand('additionalResult($expand=user),attachments')
      .where('actionStatus/alternateName').equal('CompletedActionStatus')
      .or('actionStatus/alternateName').equal('FailedActionStatus')
      .or('actionStatus/alternateName').equal('ActiveActionStatus')
      .prepare().and('additionalResult').notEqual(null)
      .orderByDescending('additionalResult/dateCreated')
      .take(-1)
      .getItems();
  }

  async getExamParticipants (courseExam) {
    // get course exam participants
    const results = await this._context.model(`/instructors/me/exams/${courseExam}/participants`)
      .asQueryable()
      .select('agree,count(student) as count')
      .groupBy('agree')
      .getList();
    const participants = {
      'total': 0,
      'agree': 0,
      'decline': 0
    };
    if (results && results.value) {
      const agree = results.value.find(x => {
        return x.agree === true;
      });
      participants.agree = agree ? agree.count : 0;
      const decline = results.value.find(x => {
        return x.agree === false;
      });
      participants.decline = decline ? decline.count : 0;
      participants.total = participants.decline + participants.agree;
    }
    return participants;
  }
  /**
   *
   * Gets the last upload actions of the user.
   * The upload cactions are the completed, active or failed.
   *
   * @param {number} number The maximum number of items that the function can fetch (optional, default=3)
   *
   */
  getUploadHistoryRecent(number: number = 3): Promise<any> {
    return this._context.model(`/instructors/me/uploadActions`)
      .asQueryable()
      .expand('object')
      .where('actionStatus/alternateName').equal('CompletedActionStatus')
      .or('actionStatus/alternateName').equal('ActiveActionStatus')
      .or('actionStatus/alternateName').equal('FailedActionStatus')
      .orderByDescending('dateCreated')
      .expand('object($expand=course, examPeriod, status, year, classes($expand=courseClass)),additionalResult')
      .take(number)
      .getItems();
  }

  getGradesStatistics(courseExamId): any {
    return this._context.model(`/instructors/me/exams/${courseExamId}/students?$count=true`)
      .asQueryable()
      .groupBy('isPassed,examGrade,formattedGrade')
      .select('count(id) as count,isPassed,examGrade,formattedGrade')
      .take(-1)
      .getList();
  }

  signCourseExamDocument(courseExamId, uploadID, additionalResult): any {
    return this._context.model(`instructors/me/exams/${courseExamId}/actions/${uploadID}/sign`)
      .save(additionalResult);
  }

  cancelCourseExamDocument(courseExamId, uploadID): any {
    return this._context.model(`instructors/me/exams/${courseExamId}/actions/${uploadID}/cancel`)
      .save(null);
  }

  /**
   * Prints a report template
   * @param {number} id
   * @param {*} reportParams
   */
  async printReport(id: number, reportParams: any): Promise<Blob> {
    const headers = new Headers({
      'Accept': 'application/pdf',
      'Content-Type': 'application/json'
    });
    // get service headers
    const serviceHeaders = this._context.getService().getHeaders();
    // manually assign service headers
    Object.keys(serviceHeaders).forEach((key) => {
      if (Object.prototype.hasOwnProperty.call(serviceHeaders, key)) {
        headers.set(key, serviceHeaders[key]);
      }
    });
    // get print url
    const printURL = this._context.getService().resolve(`instructors/me/reports/${id}/print`);
    // get report blob
    return this._http.post(printURL, reportParams, {
      headers: this._context.getService().getHeaders(),
      responseType: 'blob',
      observe: 'response'
    }).toPromise().then( (response) => {
      const contentLocation = response.headers.get('content-location');
      if (contentLocation != null) {
        Object.defineProperty(response.body, 'contentLocation', {
          configurable: true,
          enumerable: true,
          writable: true,
          value: contentLocation
        });
      }
      return response.body;
    });
  }

  getReportTemplates(): any {
    // get available report templates
    return this._context.model(`/instructors/me/reports`)
      .asQueryable()
      .expand('reportCategory')
      .getItems();
  }
}
