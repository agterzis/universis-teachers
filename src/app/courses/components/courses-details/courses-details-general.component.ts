import { Component, OnInit, OnDestroy } from '@angular/core';
import { CoursesService } from '../../services/courses.service';
import {ActivatedRoute} from '@angular/router';
import {ErrorService} from '@universis/common';
import {LoadingService} from '@universis/common';
import {template} from 'lodash';
import {ProfileService} from '../../../profile/services/profile.service';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-courses-details-general',
  templateUrl: './courses-details-general.component.html',
  styleUrls: ['./courses-details.component.scss']
})
export class CoursesDetailsGeneralComponent implements OnInit, OnDestroy {
  public selectedClass: any;
  public isLoading = true;        // Only if data is loaded
  private routeParams: any;
  private fragmentSubscription: Subscription;
  private paramsSubscription: Subscription;

  constructor(private coursesService: CoursesService,
              private errorService: ErrorService,
              private loadingService: LoadingService,
              private route: ActivatedRoute,
              private _profileService: ProfileService) {
  }

  ngOnInit() {
    // show loading
    this.loadingService.showLoading();
    this.paramsSubscription = this.route.parent.params.subscribe(routeParams => {
      this.routeParams = routeParams;
      this.loadData();
    });

    this.fragmentSubscription = this.route.fragment.subscribe(fragment => {
      if (fragment && fragment === 'reload') {
        this.loadData();
      }
    });
  }

  loadData() {
    const { course, year, period } = this.routeParams;
    this.coursesService.getCourseClass(course, year, period).then(courseClass => {
      // set selected course class
      this.selectedClass = courseClass;
      // get class and elearning url from instructor/department/organization/instituteConfiguration
      this._profileService.getInstructor().then(instructor => {
        if (instructor && instructor.department && instructor.department.organization &&
          instructor.department.organization.instituteConfiguration) {
          const instituteConfig = instructor.department.organization.instituteConfiguration;
          this.selectedClass.classUrl = instituteConfig.courseClassUrlTemplate ?
            template(instituteConfig.courseClassUrlTemplate)(this.selectedClass) : null;
          this.selectedClass.eLearningUrl = instituteConfig.eLearningUrlTemplate ?
            template(instituteConfig.eLearningUrlTemplate)(this.selectedClass) : null;
        }

        // hide loading
        this.loadingService.hideLoading();
        this.isLoading = false;
      });
    }).catch(err => {
      // hide loading
      this.loadingService.hideLoading();
      return this.errorService.navigateToError(err);
    });
  }

  public openUrl(url) {
    if (url) {
      window.open(url, '_blank');
    }
  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
  }

}
