import {CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {environment} from '../../environments/environment';
import {BsDropdownModule, BsModalRef, BsModalService, ModalBackdropComponent, BsDropdownDirective, TooltipModule} from 'ngx-bootstrap';
import {CoursesRoutingModule} from './courses-routing.module';
import {CoursesHomeComponent} from './components/courses-home/courses-home.component';
import {CoursesRecentComponent} from './components/courses-recent/courses-recent.component';
import {CoursesDetailsComponent} from './components/courses-details/courses-details.component';
import {CoursesDetailsTabsComponent} from './components/courses-details-tabs/courses-details-tabs.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CoursesDetailsGeneralComponent} from './components/courses-details/courses-details-general.component';
import {CoursesDetailsGradingComponent} from './components/courses-details/courses-details-grading.component';
import {CoursesDetailsStudentsComponent} from './components/courses-details/courses-details-students.component';
import {CoursesHistoryComponent} from './components/courses-history/courses-history.component';
import { NgPipesModule} from 'ngx-pipes';
import { CoursesGradesModalComponent } from './components/courses-grades-modal/courses-grades-modal.component';
import { CoursesGradesStep1Component } from './components/courses-grades-modal/courses-grades-step-1.component';
import { CoursesGradesStep2Component } from './components/courses-grades-modal/courses-grades-step-2.component';
import { CoursesGradesStep3Component } from './components/courses-grades-modal/courses-grades-step-3.component';
import {AppEventService, SessionUserStorageService, SharedModule, UserStorageService, ModalService} from '@universis/common';
import {CoursesSharedModule} from './courses-shared.module';
import {TabsModule} from 'ngx-bootstrap';
import {CoursesDetailsExamComponent} from './components/courses-details/course-details-exam.component';
import {ChartsModule} from 'ng2-charts';
import {InfiniteScrollModule} from 'ngx-infinite-scroll';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import {TeachersSharedModule} from '../teachers-shared/teachers-shared.module';
import {CoursesDetailsGradingIndexComponent} from './components/courses-details/courses-details-grading-index';
import {MessagesSharedModule} from '../messages/messages.shared';
import {SendMessageToClassComponent} from '../messages/components/send-message-to-class/send-message-to-class.component';
import {CoursesDetailsExamParticipantsComponent} from './components/courses-details/course-details-exam-participants.component';
import {EventsModule} from '@universis/ngx-events';
import { COLUMN_FORMATTERS } from '@universis/ngx-tables';
import {ConfigurationService} from '@universis/common';
import {
  ActionLinkFormatter,
  LinkFormatter,
  ButtonFormatter,
  LanguageFormatter,
  DateTimeFormatter,
  TemplateFormatter,
  TrueFalseFormatter,
  TranslationFormatter,
  NgClassFormatter,
  NestedPropertyFormatter,
  SelectRowFormatter,
  AdvancedColumnFormatter
} from '@universis/ngx-tables';
import { DateFormatter, AbsenceFormatter, StatusFormatter } from '@universis/ngx-events';
import { FORMATTERS } from '@universis/ngx-tables';
import {
  DeleteEventModelResolver,
  EventAbsencesResolver,
  EventConfigResolver,
  EventCourseClassInstructorResolver,
  EventCourseClassResolver, EventCourseClassSectionEndpointResolver, EventModelResolver,
  ShowActionButtonResolver
} from './event-configs.resolver';
import {AngularDataContext, MostModule} from '@themost/angular';
import {ProfileService} from '../profile/services/profile.service';
import { TablesModule } from '@universis/ngx-tables';
import { ExamsNewGradeSubmissionComponent } from './components/exams-new-grade-submission/exams-new-grade-submission.component';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { NgxSignerModule } from '@universis/ngx-signer';
import { CourseClassResolver } from './course-class.revolver';
import { QaModule } from '@universis/ngx-qa';
// tslint:disable-next-line:import-spacing
import  {CoursesExamsHistoryComponent} from './components/courses-exams-history/courses-exams-history.component';
import { RouterModalModule } from '@universis/common/routing';
import { CourseClassAbsenceLimitComponent } from './components/courses-details/course-class-absence-limit.component';

@NgModule({
    imports: [
        ChartsModule,
        CommonModule,
        FormsModule,
        CoursesRoutingModule,
        CoursesSharedModule,
        TranslateModule,
        SharedModule,
        BsDropdownModule,
        NgPipesModule,
        ReactiveFormsModule,
        TabsModule.forRoot(),
        InfiniteScrollModule,
        TooltipModule,
        TeachersSharedModule,
        MessagesSharedModule,
        EventsModule,
        MostModule,
        TablesModule,
        NgxDropzoneModule,
        NgxSignerModule.forRoot(),
        QaModule,
        RouterModalModule
    ],
  declarations: [
    CoursesHomeComponent,
    CoursesRecentComponent,
    CoursesDetailsComponent,
    CoursesDetailsGradingComponent,
    CoursesDetailsStudentsComponent,
    CoursesDetailsTabsComponent,
    CoursesDetailsGeneralComponent,
    CoursesDetailsExamComponent,
    CoursesHistoryComponent,
    CoursesGradesModalComponent,
    CoursesGradesStep1Component,
    CoursesGradesStep2Component,
    CoursesGradesStep3Component,
    CoursesDetailsGradingIndexComponent,
    CoursesDetailsExamParticipantsComponent,
    ExamsNewGradeSubmissionComponent,
    CoursesExamsHistoryComponent,
    CourseClassAbsenceLimitComponent
  ],
  providers: [
    CoursesGradesModalComponent,
    BsDropdownDirective,
    SharedModule,
    BsModalRef,
    {
      provide: COLUMN_FORMATTERS,
      useFactory: extendedFormattersFactory,
      deps: [ConfigurationService]
    },
    {
      provide: CourseClassResolver,
      useFactory: courseClassResolver,
      deps: [AngularDataContext]
    },
    {
      provide: EventAbsencesResolver,
      useFactory: eventAbsenceResolver,
      deps: [AngularDataContext]
    },
    {
      provide: EventConfigResolver,
      useFactory: eventConfigResolver,
      deps: [AngularDataContext]
    },
    {
      provide: EventCourseClassResolver,
      useFactory: eventCourseClassResolver,
      deps: [AngularDataContext]
    },
    {
      provide: EventCourseClassInstructorResolver,
      useFactory: eventCourseClassInstructorResolver,
      deps: [ProfileService]
    },
    {
      provide: EventCourseClassSectionEndpointResolver,
      useFactory: eventCourseClassSectionEndpointResolver,
      deps: [AngularDataContext]
    },
    {
      provide: ShowActionButtonResolver,
      useFactory: eventActionButtonResolver,
      deps: [AngularDataContext]
    },
    {
      provide: UserStorageService,
      useClass: SessionUserStorageService
    },
    EventModelResolver,
    DeleteEventModelResolver,
    AppEventService,
    ModalService
  ],
  entryComponents: [
    CoursesGradesModalComponent,
    SendMessageToClassComponent
  ],
  exports: [
    CoursesGradesModalComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
})
export class CoursesModule {
  constructor(private _translateService: TranslateService) {
    //
  }
}

export function extendedFormattersFactory(configService: ConfigurationService) {
  return {...FORMATTERS,
    DateFormatter: new DateFormatter(configService),
    AbsenceFormatter: new AbsenceFormatter(),
    StatusFormatter: new StatusFormatter(configService)};
}
export function eventAbsenceResolver(context: AngularDataContext) {
  return new EventAbsencesResolver(context);
}

export function eventConfigResolver(context: AngularDataContext) {
  return new EventConfigResolver(context);
}

export function eventCourseClassResolver(context: AngularDataContext) {
  return new EventCourseClassResolver(context);
}

export function eventCourseClassInstructorResolver(profile: ProfileService) {
  return new EventCourseClassInstructorResolver(profile);
}

export function eventCourseClassSectionEndpointResolver(context: AngularDataContext) {
  return new EventCourseClassSectionEndpointResolver(context);
}

export function eventActionButtonResolver(context: AngularDataContext) {
  return new ShowActionButtonResolver(context);
}

export function courseClassResolver(context: AngularDataContext) {
  return new CourseClassResolver(context);
}

