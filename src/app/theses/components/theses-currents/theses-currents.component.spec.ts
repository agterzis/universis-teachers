import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThesesCurrentsComponent } from './theses-currents.component';
import { SharedModule } from '@universis/common';
import { TranslateModule } from '@ngx-translate/core';
import { MostModule } from '@themost/angular';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ModalModule } from 'ngx-bootstrap';
import { ErrorModule } from '@universis/common';
import { RouterTestingModule } from '@angular/router/testing';

describe('ThesesCurrentsComponent', () => {
  let component: ThesesCurrentsComponent;
  let fixture: ComponentFixture<ThesesCurrentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        SharedModule.forRoot(),
        TranslateModule.forRoot(),
        ModalModule.forRoot(),
        ErrorModule.forRoot(),
        RouterTestingModule,
        MostModule.forRoot({
          base: '/',
          options: {
              useMediaTypeExtensions: false
          }
        }),
        HttpClientTestingModule
      ],
      declarations: [ ThesesCurrentsComponent ],
      providers: [
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThesesCurrentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
